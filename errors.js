function createErrorType(name, code, default_message) {
    function NewErrorType(message) {
        this.name = name;
        this.message = message || default_message;
        this.status = code;
        this.stack = (new Error()).stack;
    }
    NewErrorType.prototype = Object.create(Error.prototype);
    NewErrorType.prototype.constructor = NewErrorType;
    return NewErrorType;
};

var errors = {
    NotFoundError: createErrorType('NotFoundError', 404, 'Not found'),
    BadRequestError: createErrorType('BadRequestError', 400, 'Bad request'),
    ValidationError: createErrorType('ValidationError', 422, 'Unprocessable entity'),
    ForbiddenError: createErrorType('ForbiddenError', 403, 'Forbidden'),    
    UnauthorizedError: createErrorType('UnauthorizedError', 401, 'Unauthorized'),    
    InternalError: createErrorType('InternalError', 500, 'Unauthorized')
};

module.exports = errors;
