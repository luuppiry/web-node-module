var _ = require('lodash');

var hasOneOfRoles = function (user, roleIds) {
    var userRoleIds = _.map(user.roles, 'id');

    var commonRoles = _.intersection(userRoleIds, roleIds);

    return commonRoles.length > 0;
};

var getSuperiorRolesForRole = function (role) {
    var superiorIds = [];

    while (role.superior) {
        superiorIds.push(role.superior);

        role = role.superior;
    }

    return superiorIds;
};

module.exports.UserMustHaveOneOfRoles = function (roleIds) {
    return function (req, res, next) {
        if (req.user && hasOneOfRoles(req.user, roleIds)) {
            next();
        } else {
            res.status(403).end('forbidden');
        }
    }
}
