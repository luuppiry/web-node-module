module.exports = function initialize(configure, config) {
    var Knex = require('knex');
    var knex = Knex(config.knex);
    var Model = require('./model');

    Model.knex(knex);
    
    var App = require('./app');
    var Port = require('./port');

    Port(App(configure, config.app));
};

