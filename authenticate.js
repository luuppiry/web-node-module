var rp = require('request-promise');

module.exports.FetchUserMiddleware = function () {
    return function (req, res, next) {
        req.user = null;
        if (req.token) {
            var options = {
                uri: 'http://' + process.env.SERVICE_ADDRESS + '/service/users/users/me',
                headers: {
                    'Authorization': 'Bearer ' + req.token
                },
                json: true
            };

            rp(options)
                    .then(function (user) {
                        req.user = user;
                        next();
                    })
                    .catch(function () {
                        next();
                    });
        } else {
            next();
        }
    }
}
