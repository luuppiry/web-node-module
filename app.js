var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var errors = require('./errors');
var bearerToken = require('express-bearer-token');
var Model = require('./model');
var knexLogger = require('knex-logger');
var config = require('knex-logger');

module.exports = function (configure, configuration) {
    var app = express();

    app.use(logger('dev'));
    app.use(knexLogger(Model.knex()));
    app.use(bearerToken());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));

    configure(app);

    app.use(function (req, res, next) {
        var err = new errors.NotFoundError();
        next(err);
    });

    if (configuration.showStackTraces) {
        app.use(function (err, req, res, next) {
            console.log(err.stack);

            res.status(err.status || 500);
            res.json({
                name: err.name,
                message: err.message,
                error: err
            });
        });
    } else {
        // production error handler
        // no stacktraces leaked to user
        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            res.json({
                name: err.name,
                message: err.message,
                error: {}
            });
        });
    }
    
    return app;
}

